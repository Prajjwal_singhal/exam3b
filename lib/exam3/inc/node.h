//
// Created by babbu on 07-12-2017.
//

#ifndef CMPE126F17_EXAM3A_NODE_H
#define CMPE126F17_EXAM3A_NODE_H
class node
{
public:
    int data;
    unsigned freq;
    node *next ;

    node(int temp)
    {
        data = temp;
        freq = 1;
        next = nullptr;
    }
};
#endif //CMPE126F17_EXAM3A_NODE_H
