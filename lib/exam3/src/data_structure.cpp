#include "data_structure.h"
#include <string>
#include "node.h"
data_structure::data_structure() {
    // Default constructor: Generate an empty data structure
    head = nullptr;
}

data_structure::data_structure(std::string input_string) {
    // String constructor: Construct a data structure and store the input string into it
    std::string temp = "";
    int var;
    node *temp_add,*temp_search;


    int i=0;
    for(i=0;input_string[i]!=',';i++)
    {
        temp = temp + input_string[i];
    }
    head = new node(stoi(temp));
    temp = "";
    i++;
    temp_add = this->head;
    for(;input_string[i]!='\0';i++)
    {
        bool add_element_1 = false;
        if(input_string[i]==',')
        {
            add_element_1 = true;

        }
        else
        {
            temp = temp + input_string[i];
        }
        if(add_element_1)
        {
            bool add_element=true;
            temp_search = this->head;
            while(temp_search!=nullptr)
            {
                if(temp_search->data == stoi(temp)) {
                    add_element = false;
                    temp_search->freq++;
                    temp = "";
                    break;
                }
                else
                    temp_search = temp_search->next;
            }
            if(add_element)
            {
                temp_add->next = new node(stoi(temp));
                temp_add = temp_add->next;
                temp = "";
            }
        }
    }
    bool add_element=true;
    temp_search = this->head;
    while(temp_search!=nullptr)
    {
        if(temp_search->data == stoi(temp)) {
            add_element = false;
            temp_search->freq++;
            temp = "";
            break;
        }
        else
            temp_search = temp_search->next;
    }
    if(add_element)
    {
        temp_add->next = new node(stoi(temp));
        temp_add = temp_add->next;
        temp = "";
    }

}

data_structure::~data_structure() {
    // Default Destructor: Deconstruct the data structure
    node *temp;
    temp = head;
    while(head!= nullptr)
    {
        head = head->next;
        delete temp;
        temp = head;
    }
}

unsigned int data_structure::frequency(int input_character) {
    // Return the number of times the integer is in the data structure.
    node *temp;
    int return_val = 0;
    temp = head;
    bool found = false;
    while(!found&&temp!= nullptr)
    {
        if(temp->data == input_character)
        {
            return_val = temp->freq;
            found = true;
        }
        temp = temp->next;
    }
    return return_val;
}

int data_structure::most_frequent() {
    // Return the most frequent number in the data structure. If there is more than one, return the highest value
    node *return_val;
    node *temp;
    temp = head;
    if(temp== nullptr)
        return 0;
    return_val = temp;
    temp = temp->next;
    while(temp != nullptr)
    {

        if(return_val->freq < temp->freq)
            return_val = temp;
        else if(return_val->freq == temp->freq)
        {
            if(return_val->data > temp->data)
            return_val = temp;
        }
        temp = temp->next;
    }
    return return_val->data;
}

int data_structure::least_frequent() {
    // Return the least frequent number in the data structure. If there is more than one, return the lowest value
    node *return_val;
    node *temp;
    temp = head;
    if(temp== nullptr)
        return 0;
    return_val = temp;
    temp = temp->next;
    while(temp != nullptr)
    {

        if(return_val->freq > temp->freq)
            return_val = temp;
        else if(return_val->freq == temp->freq)
        {
            if(return_val->data  < temp->data)
                return_val = temp;
        }
        temp = temp->next;
    }
    return return_val->data;

}

void data_structure::sort() {
    // Sort the data structure first by frequency, greatest to least and then by value, least to greatest.
    // Example: 1:3,42:4,17:3,11:1,46:1,3:2         sorted: 42:4,1:3,17:3,3:2,11:1,46:1

    node *temp,*current_next,*current;
    temp = head;
    current = head;
    if(current== nullptr || current->next==nullptr)
        return;
    for(current = temp;current!= nullptr;current = current->next)
        {
            for(current_next=current->next;current_next!= nullptr;current_next = current_next->next)
            {
                if(current->freq < current_next->freq)
                {
                    unsigned temp = current->data;
                    int temp1= current->freq;
                    current->freq = current_next->freq;
                    current->data = current_next->data;
                    current_next->freq = temp1;
                    current_next->data = temp;
                }
                else if(current->freq == current_next->freq)
                {
                    if(current->data > current_next->data)
                    {
                        unsigned temp = current->data;
                        int temp1= current->freq;
                        current->freq = current_next->freq;
                        current->data = current_next->data;
                        current_next->freq = temp1;
                        current_next->data = temp;
                    }
                }
            }
        }
}

std::istream &operator>>(std::istream &stream, data_structure &structure) {
    // Stream in a string, empty the current structure and create a new structure with the new streamed in string.
    std::string s1;
    stream>>s1;
    data_structure temp;
    temp.head = structure.head;
    data_structure temp2(s1);
    structure.head = temp2.head;
    temp2.head = nullptr;
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const data_structure &structure) {
    // Stream out the data structure
    // Output in this format "<integer>:<frequency>,<integer>:<frequency>,<integer>:<frequency>"
    node *temp;
    temp = structure.head;
    if(temp!= nullptr)

    {
        while(temp->next!= nullptr)
        {
            stream<<temp->data<<":"<<temp->freq<<",";
            temp = temp->next;
        }
        stream<<temp->data<<":"<<temp->freq;
    }

    return stream;
}
